import React, { useState, useEffect } from 'react';
import { FaBars, FaTimes } from 'react-icons/fa';
import { IconContext }  from 'react-icons/lib';
import { keyframes } from 'styled-components';
import { Nav, NavbarContainer, NavLogo, NavIcon, MobileIcon, NavMenu, NavItem, NavLinks } from './NavbarElements'

const Navbar = () => {
    const [click, setClick] = useState(false);
    const [scroll, setScroll] = useState(false);

    const handleClick = () =>  setClick(!click)

    const changeNav = () => {
        if (window.scrollY >= 80) {
            setScroll(true)
        } else {
            setScroll(false)
        }
    }

    useEffect(() => {
        changeNav()
            window.addEventListener("scroll", changeNav)
    }, [])

    return (
        <>
            <Nav active={scroll} click={click}>
                <NavbarContainer>
                     <NavLogo to="/">
                        <NavIcon />
                        Nature
                    </NavLogo>
                    <MobileIcon onClick={handleClick}>{click ? <FaTimes /> : <FaBars />}
                    </MobileIcon>
                    <NavMenu onClick={handleClick} click={click}>
                    <NavItem>
                            <NavLinks to="/">Home</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="/images">Images</NavLinks>
                        </NavItem>
                        <NavItem>
                            <NavLinks to="/destinations">Destinations</NavLinks>
                        </NavItem>
                    </NavMenu>     
                </NavbarContainer>
            </Nav> 
        </>
    )
}

export default Navbar;
