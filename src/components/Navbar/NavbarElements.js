import styled from 'styled-components';
import {GiHolyOak} from 'react-icons/gi';
import {Link} from 'gatsby';
import {keyframes} from 'styled-components';

export const Nav = styled.nav`
    background: ${({active}) => active ? "#fff" : "transparent"};
    height: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 1rem;
    position: sticky;
    top: 0;
    z-index: 999;

    @media screen and (max-width: 960px) {
        background: ${({ click }) => (click ? "black" : "transparent")};
    }
`;

export const NavbarContainer = styled.div`
    display: flex;
    justify-content: space-between;
    height: 80px;
    z-index: 1;
    width: 100%;
    max-width: 1000px;
`;

export const NavLogo = styled(Link)`
    color: white;
    justify-self: flex-start;
    cursor: pointer;
    text-decoration: none;
    font-size: 2.5rem;
    display: flex;
    align-items: center;
    font-weight: 300;

    &:hover {
            transform: scale(1.10);
            transition: all 0.3s ease-in-out;
    }
`;

export const NavIcon = styled(GiHolyOak)`
    margin: 0 1rem 0 2rem;
    align: center;
    margin-bottom: 0px;
    
`;

export const MobileIcon = styled.div`
    align: center;
    display: none;
    color: white;

    @media screen and (max-width: 960px) {
        display: block;
        position: absolute;
        top: 0;
        right: 0;
        transform: translate(-100%, 60%);
        font-size: 1.8rem;
        cursor: pointer;
        z-index: 3;
    }
`;

export const NavMenu = styled.ul`
    display: flex;
    align-items: center;
    list-style: none;
    text-align: center;

    @media screen and (max-width: 960px) {
        display: flex;
        flex-direction: column;
        width: 100%;
        height: 100vh;
        position: absolute;
        top: ${({ click}) => (click ? "100%" : "-1000px")};
        opacity: 100;
        transition: all 0.3 ease;
        background: black;
        z-index: 4;
        padding-top: 80px;
    }
`;

export const NavItem = styled.li`
    height: 80px;

    @media screen and (max-width: 960px) {
        width: 100%;
    }
`;

export const NavLinks = styled(Link)`
    color: white;
    display: flex;
    align-items: center;
    text-decoration: none;
    padding: 0.5rem 1rem;
    height: 100%;
    font-size: 2rem;

    &:hover {
            color: white;
            transform: scale(1.10);
            transition: all 0.3 ease;
            border-bottom: 2px solid white;

            @media screen and (max-width: 960px) {
            border-bottom: none;
        };
     };
    
    @media screen and (max-width: 960px) {
        text-align: center;
        padding: 2rem;
        width: 100%;
        display: table;
        justify-content: center;
        z-index: 3;
    };
`;

