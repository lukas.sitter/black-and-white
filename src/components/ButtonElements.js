import styled from 'styled-components';

export const Button = styled.button`
    border-radius: 4px;
    background: ${({ primary }) => (primary ? 'black' : 'white')};
    white-space: nowrap;
    padding: ${({ big }) => (big ? '16px 64px' : '10px 20px')};
    color: black;
    font-size: ${({ fontBig }) => (fontBig ? '20px' : '16px')};
    outline: none;
    border: none;
    cursor: pointer;
    margin-top: 15px;
    font-weight: 600;

    &:hover {
        transition: all 0.3s ease-out;
        background: ${({ primary }) => (primary ? 'black' : 'white')};
        transform: scale(1.10);
        color: black;
    }

    @media screen and (max-width: 960px) {
        width: 100%;
    }
`;

