/* eslint-disable no-sequences */
import React, { useState } from 'react';
import Video from '../videos/video.mp4';
import {
  HeroContainer, HeroBg, VideoBg, HeroContent, HeroH1, HeroP, HeroBtnWrapper, ArrowForward, ArrowRight,
} from './HeroElements';
import { Button} from '..//ButtonElements';

const HeroSection = () => {
  const [hover, setHover] = useState(false);

  const onHover = () => {
    setHover(!hover);
  };

  return (
    <HeroContainer id="home">
      <HeroBg>
        <VideoBg autoPlay loop muted src={Video} type="video/mp4" />
      </HeroBg>
      <HeroContent>
        <HeroH1>Nature</HeroH1>
        <HeroP>In black and white</HeroP>
        <Button>Get started</Button>
      </HeroContent>
    </HeroContainer>
  );
};

export default HeroSection;
